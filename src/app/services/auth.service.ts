import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loginEmitter: EventEmitter<any> = new EventEmitter();
  isLogged: boolean = false;

  constructor(private http: HttpClient, private router: Router) { }

  login(username: string, password: string){
    const url = 'https://reqres.in/api/login';
    const data = {
      'email': username,
      'password': password
    };
    return this.http.post(url, data).subscribe(
      r=>{
        this.router.navigateByUrl('/home');
        this.loginEmitter.emit('Esta Logueado');
        this.isLogged = true;
      },
      err=>{
        console.error('Datos Incorrectos,' + err.error.error);
      }
    );
  }

  logout(){
    this.loginEmitter.emit('');
    this.isLogged = false;
  }
}
