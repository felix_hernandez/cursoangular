import { Injectable } from '@angular/core';
import { Employee } from '../models/employee.model';
import { Product } from '../models/products.model';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  employeesList: Array<Employee>;
  productsList: Array<Product>;
  usersList: Array<User>;

  constructor(private http: HttpClient) { }

  getEmployees(){
    return this.employeesList;
  }
  
  getEmployee(index){
    return this.employeesList[index];
  }

  setEmployee(employee: Employee){
    if (!this.employeesList){
      this.employeesList = [];
    }
    this.employeesList.push(employee);
  }

  deleteEmployee(index: number){
    this.employeesList.splice(index, 1);
  }

  updateEmployee(index, employee){
    this.employeesList[index] = employee;
  }

  // PRODUCTS
  getProducts(){
    return this.productsList;
  }

  getProduct(index){
    return this.productsList[index];
  }

  setProduct(product: Product){
    if (!this.productsList){
      this.productsList = [];
    }
    this.productsList.push(product);
  }

  deleteProduct(index: number){
    this.productsList.splice(index, 1);
  }

  updateProduct(index, product){
    this.productsList[index] = product;
  }

  getUsers(){
    return this.http.get('https://reqres.in/api/users');
  }
}
