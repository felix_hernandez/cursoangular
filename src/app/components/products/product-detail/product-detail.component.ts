import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/products.model';
import { DataService } from 'src/app/services/data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product: Product;
  products: Array<Product>;
  productId: string;

  constructor(private dataService: DataService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.product = new Product('','',0,true);
    this.productId = this.route.snapshot.paramMap.get('id');
    if (this.productId){
      this.product = this.dataService.getProduct(this.productId);
    }
  }

  getProducts(){
    this.products = this.dataService.getProducts();
  }

  addProduct(){
    this.dataService.setProduct(this.product);
    this.getProducts();
    this.router.navigate(['/products/product-list']);
  }

  updateProduct(){
    this.dataService.updateProduct(this.productId, this.product);
    this.getProducts();
    this.router.navigate(['/products/product-list']);
  }

}
