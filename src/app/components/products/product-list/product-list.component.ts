import { Component, OnInit } from '@angular/core';
import { Product } from '../../../models/products.model';
import { DataService } from '../../../services/data.service';
import {ConfirmationService} from 'primeng/api';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  product: Product;
  products: Array<Product>;

  constructor(private dataService: DataService, private confirmation: ConfirmationService) { }

  ngOnInit(): void {
    this.product = new Product('', '', 0, true);
    this.getProducts();
  }

  getProducts(){
    this.products = this.dataService.getProducts();
  }

  deleteProduct(index: number){
    this.dataService.deleteProduct(index);
    this.getProducts();
  }
  addProduct(){
    this.dataService.setProduct(this.product);
    this.getProducts();
  }

  confirm(i) {
    this.confirmation.confirm({
        message: 'Seguro que deseas borrar el producto?',
        accept: () => {
            this.deleteProduct(i);
        }
    });
  }

}
