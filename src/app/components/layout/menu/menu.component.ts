import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  user: any;
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.loginEmitter.subscribe(login => {
      this.user = login;
      console.log(login);
    });
  }

  logout(){
    this.authService.logout();
  }

}
