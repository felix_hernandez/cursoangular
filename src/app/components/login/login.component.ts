import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  countries: Array<string> = ['Mexico', 'USA', 'Canada'];

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  login(){
    console.log(this.username + ' - '+ this.password)
    this.auth.login(this.username, this.password);
  }

  keyUp(e){
    console.log(e.key);
    console.log(e.target.value);
  }

  change(e){

  }

}
