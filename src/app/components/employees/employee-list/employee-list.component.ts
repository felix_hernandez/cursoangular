import { Component, OnInit } from '@angular/core';
import { Employee } from '../../../models/employee.model';
import { DataService } from '../../../services/data.service';
import {ConfirmationService} from 'primeng/api';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employee: Employee;
  employeeName: string;
  employees : Array<Employee>;

  constructor(private dataService: DataService, private confirmation: ConfirmationService) { }

  ngOnInit(): void {
    this.employee = new Employee('', '', '')
    this.getEmployees();
  }

  getEmployees(){
    this.employees = this.dataService.getEmployees();
  }  

  deleteEmployee(index: number){
    this.dataService.deleteEmployee(index);
    this.getEmployees();
  }
  addEmployee(){
    this.dataService.setEmployee(this.employee);
    console.log(this.employeeName);
    this.getEmployees();
  }

  confirm(i) {
    this.confirmation.confirm({
        message: 'Seguro que deseas borrar el empleado?',
        accept: () => {
            this.deleteEmployee(i);
        }
    });
}
}
