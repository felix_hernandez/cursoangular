import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee.model';
import { DataService } from 'src/app/services/data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  employee: Employee;
  employees: Array<Employee>;

  employeeId: string;

  constructor(private dataService: DataService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.employee = new Employee('','','');
    this.employeeId = this.route.snapshot.paramMap.get('id');
    if (this.employeeId) {
      this.employee = this.dataService.getEmployee(this.employeeId);
    }

  }

  getEmployees(){    
    this.employees = this.dataService.getEmployees();
  }

  addEmployee(){
    this.dataService.setEmployee(this.employee);
    this.getEmployees();
    this.router.navigate(['/employees/employee-list'])
  }
  updateEmployee(){
    this.dataService.updateEmployee(this.employeeId, this.employee);
    this.getEmployees();
    this.router.navigate(['/employees/employee-list'])
  }

}
