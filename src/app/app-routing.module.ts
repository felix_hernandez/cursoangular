import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Componentes
import { HomeComponent } from './components/home/home.component'
import { EmployeesComponent } from './components/employees/employees.component';
import { EmployeeListComponent } from './components/employees/employee-list/employee-list.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductDetailComponent } from './components/products/product-detail/product-detail.component';
import { ProductListComponent } from './components/products/product-list/product-list.component';
import { SettingsComponent } from './components/settings/settings.component';
import { UsersComponent } from './components/users/users.component';
import { UserDetailComponent } from './components/users/user-detail/user-detail.component';
import { UserListComponent } from './components/users/user-list/user-list.component';
import { EmployeeDetailComponent } from './components/employees/employee-detail/employee-detail.component';
import { LoginComponent } from './components/login/login.component';
import { SecurityGuard } from './services/security.guard';


const routes: Routes = [
  {path:'', redirectTo: '/home', pathMatch:'full'},
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'employees', component: EmployeesComponent, children: [
    {path:'employee-detail', component: EmployeeDetailComponent},
    {path:'employee-detail/:id', component: EmployeeDetailComponent},
    {path:'employee-list', component: EmployeeListComponent},
  ]},
  {path: 'products', component: ProductsComponent, children: [
    {path:'product-detail', component: ProductDetailComponent},
    {path:'product-detail/:id', component: ProductDetailComponent},
    {path:'product-list', component: ProductListComponent},
  ]},
  {path: 'users', component: UsersComponent, children: [
    {path:'user-detail/:id', component: UserDetailComponent},
    {path:'user-detail', component: UserDetailComponent},
    {path:'user-list', component: UserListComponent, canActivate:[SecurityGuard]},
  ]},
  {path: 'settings', component: SettingsComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
