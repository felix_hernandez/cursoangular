export class Employee {
    name: string;
    lastName: string;
    phone: string;
    resps: Array<string>;
  
    constructor(name: string, lastName: string, phone: string, resps?: Array<string>) {
      this.name = name;
      this.lastName = lastName;
      this.phone = phone;
      this.resps = resps;
    }
  }