export class Product {
    code: string;
    description: string;
    price: number;
    active: Boolean;
    image: string;
  
    constructor(code: string, description: string, price: number, active: Boolean) {
        this.code = code;
        this.description = description;
        this.price = price;
        this.active = active;
    }
  }