import { Directive, ElementRef, Input, OnInit, ViewContainerRef, TemplateRef } from '@angular/core';

@Directive({
  selector: '[canActivate]'
})
export class CanactivateDirective implements OnInit {

  @Input('canActivate') roles: string[];
  constructor(private el: ElementRef, private c: ViewContainerRef, private t: TemplateRef<any>) {
  }
  
  ngOnInit(){
    console.log(this.roles);
    if (this.roles.includes('admin')){
      this.c.createEmbeddedView(this.t);
    }
    else{
      this.c.remove();
    }
  }

}
